﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BLM
{
    public class Category
    {
        public string ImagePath { get; set; }
        public string CategoryName { get; set; }
        public bool Selected { get; set; }

        public Category(string categoryName, string imagePath)
        {
            ImagePath = imagePath;
            CategoryName = categoryName;
            Selected = false;
        }
    }
}
