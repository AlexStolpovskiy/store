﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BLM
{
    public class Knife
    {
        public string GuidKnife { get; } =  Guid.NewGuid().ToString();
        public string LogoPath { get; set; }
        public string Name { get; set; }
        public string NewPrice { get; set; }
        public string OldPrice { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
    }
}
