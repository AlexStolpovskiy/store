﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.BLM;

namespace BLL
{
    public class KnifeManager
    {
        public List<Knife> GetAllKnifesList()
        {
           
            return ParserKnifeSingleTon.GetInstance().Knifes;
        }
        private List<Knife> GetKnivesByCategory(IEnumerable<Category> categories)
        {
            var knifeList = new List<Knife>();
            var resultKnifeList = new List<Knife>();
            knifeList = ParserKnifeSingleTon.GetInstance().Knifes;
            foreach (Category category in categories)
            {
                resultKnifeList.AddRange(knifeList.Where(x => x.Category == category.CategoryName));
            }
            return resultKnifeList;
        }

        public List<Knife> GetKnivesRandomList()
        {
        var randomItem = new Random();
            var random = new List<Knife>();
            for (var i = 0; i < 9; i++)
            {
                random.Add(ParserKnifeSingleTon.GetInstance().Knifes[randomItem.Next(0, ParserKnifeSingleTon.GetInstance().Knifes.Count)]);
            }
            return random;
        }

       

        public List<Knife> GetKnivesByCategory(string[] categories)
        {
            if (categories== null)
            {
                return GetKnivesRandomList();
            }

            var resultKnifeList = new List<Knife>();
            var knifeList = ParserKnifeSingleTon.GetInstance().Knifes;
            foreach (var category in categories)
            {
              
                resultKnifeList.AddRange(knifeList.Where(x => x.Category == category));
            }
            return resultKnifeList;

        }
        public List<Category> GetAllCategoriesForMain()
        {
          
            return ParserKnifeSingleTon.GetInstance().Categories; 


        }
        public List<Knife> GetKnifesByGuid(string[] guids)
        {
            List<Knife> knivesList = new List<Knife>();
            //TODO Change to lambda
            var list = ParserKnifeSingleTon.GetInstance().Knifes;
           foreach (string guid in guids)
            {
                foreach (var item in list)
                {
                    if (item.GuidKnife == guid)
                    {
                        knivesList.Add(item);
                    }
                }    
            }
            return knivesList;
        }
    }
}
