﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Xml.Serialization;
using BLL.BLM;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace BLL
{
    public class ParserKnifeSingleTon
    {
        private static ParserKnifeSingleTon _singleTon;

        public List<Knife> Knifes { get; set; }
        public List<Category> Categories { get; set; }


        #region Ctor
        private ParserKnifeSingleTon()
        {
            Knifes = new List<Knife>();
            Categories = new List<Category>();
            Initializecomponent();
        }
        private void Initializecomponent()
        {
#if DEBUG

            // десериализация
            var path = $"{AppDomain.CurrentDomain.BaseDirectory}Content/Knifes.xml";
            var formatter = new XmlSerializer(typeof(List<Knife>));
            using (var fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                Knifes = (List<Knife>)formatter.Deserialize(fs);
                Categories.AddRange(CategoryParse());
            }


#else
    //TODO add BeginInvoke to release version and remove load in .xml
            new Action(() => { Knifes.AddRange(ParseKnife("https://cutss.com/collections/butterfly/", "Butterfly", AppDomain.CurrentDomain.BaseDirectory)); }).Invoke();
            new Action(() => { Knifes.AddRange(ParseKnife("https://cutss.com/collections/Butterfly2/", "Butterfly2", AppDomain.CurrentDomain.BaseDirectory)); }).Invoke();
            new Action(() => { Knifes.AddRange(ParseKnife("https://cutss.com/collections/m-bayonet/", "M9-bayonet", AppDomain.CurrentDomain.BaseDirectory)); }).Invoke();
            new Action(() => { Knifes.AddRange(ParseKnife("https://cutss.com/collections/flip-knife/", "Flip-knife", AppDomain.CurrentDomain.BaseDirectory)); }).Invoke();
            new Action(() => { Knifes.AddRange(ParseKnife("https://cutss.com/collections/karambit/", "Karambit", AppDomain.CurrentDomain.BaseDirectory)); }).Invoke();
            new Action(() => { Knifes.AddRange(ParseKnife("https://cutss.com/collections/falchion/", "Falchion", AppDomain.CurrentDomain.BaseDirectory)); }).Invoke();
            new Action(() => { Knifes.AddRange(ParseKnife("https://cutss.com/collections/gut-knife/", "Gut-knife", AppDomain.CurrentDomain.BaseDirectory)); }).Invoke();
            new Action(() => { Knifes.AddRange(ParseKnife("https://cutss.com/collections/huntsman/", "Huntsman", AppDomain.CurrentDomain.BaseDirectory)); }).Invoke();
            new Action(() => { Knifes.AddRange(ParseKnife("https://cutss.com/collections/bundle-sales/", "Bundle-sales", AppDomain.CurrentDomain.BaseDirectory)); }).Invoke();
            new Action(() => { Knifes.AddRange(ParseKnife("https://www.cutss.com/collections/others", "Others", AppDomain.CurrentDomain.BaseDirectory)); }).Invoke();
            
            new Action(() => { Categories.AddRange(CategoryParse()); }).Invoke();

            //Writing all knifes to xml file
            XmlSerializer formatter = new XmlSerializer(typeof(List<Knife>));
            // получаем поток, куда будем записывать сериализованный объект
            var path = String.Format("{0}Content/Knifes.xml", AppDomain.CurrentDomain.BaseDirectory);
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, Knifes);
            }

        
#endif

        }


        public static ParserKnifeSingleTon GetInstance()
        {
            return _singleTon ?? (_singleTon = new ParserKnifeSingleTon());
        }
        #endregion

        #region MethodInitialize
        private IEnumerable<Knife> ParseKnife(string path, string category, string saveFilePath)
        {

            var knifeList = new List<Knife>();
            var htmlDoc = new HtmlWeb().Load(path);

            var findclasses = htmlDoc.DocumentNode
                .Descendants("div")
                .Where(d =>
                d.Attributes.Contains("class")
                   && d.Attributes["class"].Value.Contains("grid__item small--one-half medium-up--one-quarter"));
            foreach (var item in findclasses)
            {

                var obj = new Knife();
                obj.Category = category;
                var str = new StringBuilder();
                str.Append("https://cutss.com");
                var imgNode = item.ChildNodes[1].ChildNodes[1].ChildNodes[1];
                var nameNode = item.ChildNodes[1].ChildNodes[3].ChildNodes[1];

                //Get NewPriceNode. If doesn't exists - get only one price
                //TODO check for null. Delete try catch
                HtmlNode newPriceNode;
                try
                {
                    newPriceNode = item.ChildNodes[1].ChildNodes[3].ChildNodes[3].ChildNodes[1];
                }
                catch (ArgumentOutOfRangeException argExc)
                {
                    newPriceNode = item.ChildNodes[1].ChildNodes[3].ChildNodes[3];
                }

                //Get OldPriceNode. If doesn't exists - copy value from newPrice
                //TODO check for null. Delete try catch
                HtmlNode oldPriceNode;
                try
                {
                    oldPriceNode = item.ChildNodes[1].ChildNodes[3].ChildNodes[3].ChildNodes[3];
                }
                catch (ArgumentOutOfRangeException argumentException)
                {
                    oldPriceNode = newPriceNode;
                }
                //Get knive's name
                obj.Name = nameNode == null ? "" : nameNode.InnerText;
                //Save image file 
                if (imgNode != null)
                {
                    var clearImgName = obj.Name.Replace(" ", String.Empty).Replace("+", String.Empty);
                    str.Append(imgNode.Attributes["src"].Value);
                    obj.LogoPath = "Content/images/" + clearImgName + ".jpeg";
                  

                    SaveFile(saveFilePath + obj.LogoPath, str.ToString());
                }
                //Put prices into knive's object
                obj.NewPrice = newPriceNode == null ? "" : newPriceNode.InnerText;
                obj.OldPrice = oldPriceNode == null ? "" : oldPriceNode.InnerText;


                //Get Descriptions from other page
                var ahrefNode = item.ChildNodes[1];
                var descriptionPath = ahrefNode.GetAttributeValue("href", "");
                var descriptionDoc = new HtmlWeb().Load(descriptionPath);
                var findedDescriptions = descriptionDoc.DocumentNode
                .Descendants("div")
                .Where(d =>
                d.Attributes.Contains("class")
                   && d.Attributes["class"].Value.Contains("rte product-single__description"));
                //TODO check for null. Delete try catch
                HtmlNode descriptionsNode;
                try
                {
                     descriptionsNode = findedDescriptions.ElementAt(0).ChildNodes[3];
                }
                catch (ArgumentOutOfRangeException)
                {
                    descriptionsNode = findedDescriptions.ElementAt(0).ChildNodes[1];
                }

                //find all <p> elements
                var reviewNodes = descriptionsNode.SelectNodes("//p");
                foreach (var node in reviewNodes)
                {
                    obj.Description += " " + node.InnerText;
                }

                knifeList.Add(obj);

            }
            return knifeList;


        }
        private void SaveFile(string pathLocal, string pathUri)
        {

            byte[] imageBytes;
            var imageRequest = (HttpWebRequest)WebRequest.Create(pathUri);
            var imageResponse = imageRequest.GetResponseAsync().Result;

            var responseStream = imageResponse.GetResponseStream();

            using (var br = new BinaryReader(responseStream))
            {
                imageBytes = br.ReadBytes(500000);
            }
            responseStream.Dispose();
            imageResponse.Dispose();

            var fs = new FileStream(pathLocal, FileMode.Create);
            var bw = new BinaryWriter(fs);
            try
            {
                bw.Write(imageBytes);
            }
            finally
            {
                fs.Dispose();
                bw.Dispose();
            }
        }
        private IEnumerable<Category> CategoryParse()
        {
            var categoryList = new List<Category>
            {
                new Category("Butterfly", "Content/Images/Fade.jpeg"),
                new Category("Butterfly 2", "Content/Images/Autotronic.jpeg"),
                new Category("M9 Bayonet", "Content/Images/M9MarbleFade.jpeg"),
                new Category("Flip Knife", "Content/Images/FlipKnifeRuby.jpeg"),
                new Category("Karambit", "Content/Images/KarambitAutotronic.jpeg"),
                new Category("Falchion", "Content/Images/FalchionCrimsonWeb.jpeg"),
                new Category("Gut Knife", "Content/Images/GutKnifeMarbleFade.jpeg"),
                new Category("Huntsman", "Content/Images/HuntsmanFade.jpeg"),
                new Category("Bundle Sales", "Content/Images/SlaughterGammaCaseMedal.jpeg"),
                new Category("Others", "Content/Images/RealBloodhoundGlovesCharred.jpeg")
            };

            //Fill knife's list in each Category


            return categoryList;
        }
        #endregion







    }
}



