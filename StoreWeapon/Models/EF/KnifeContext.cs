﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace StoreWeapon.Models.EF
{
    public class KnifeContext:DbContext
    {
        public KnifeContext() : base("connection_db")
        { }

        public DbSet<KnifeForStore> KnifeForStore { get; set; }


    }
}