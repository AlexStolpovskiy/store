﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoreWeapon.Models
{
    public class KnifeForStore
    {
        public string Id { get; set; }
        public string LogoPath { get; set; }
        public string Name { get; set; }
        public string NewPrice { get; set; }
        public string OldPrice { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
    }
}