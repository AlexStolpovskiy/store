﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoreWeapon.Models
{
    public class StoreCategory
    {
        public  string Name { get; set; }
        public int Count { get; set; }
    }
}