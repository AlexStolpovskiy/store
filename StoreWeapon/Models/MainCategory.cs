﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL;
using BLL.BLM;

namespace StoreWeapon.Models
{
    public class MainCategory
    {
        public string ImagePath { get; set; }
        public string CategoryName { get; set; }

        public MainCategory(string categoryName, string imagePath)
        {
            ImagePath = imagePath;
            CategoryName = categoryName;
        }

        
    }
}