﻿$('.linkCategory').on('click', function () {
    $.ajax({
        url: "@(Url.Action("_CategoriesPanel", "Store"))",
        type: "POST",
        cache: false,
        async: false,
        data: { newCategory: "Butterfly" },
        success: function (result) {
            $("#res").html(result);
        }
    });

});