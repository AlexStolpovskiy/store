﻿function sort(index) {

    //Get HTMLCollection with prices
    var weapons = document.getElementsByClassName("block_weapon");
    var swapped;
    

    switch (index) {
        case '1': sortByName("inc");
            break;
        case '2': sortByName("dec");
            break;
        case '3': sortByPrice("inc");
            break;
        case '4': sortByPrice("dec");
            break;
    }

    function sortByPrice(value) {
        //Sort prices by Increment
        do {
            swapped = false;
            for (var i = 0; i < weapons.length - 1; i++) {
                //Get prices from childNodes
                var x = weapons[i].childNodes[1].childNodes[3].childNodes[3].childNodes[1].textContent;
                var y = weapons[i + 1].childNodes[1].childNodes[3].childNodes[3].childNodes[1].textContent;

                //Delete spaces in price
                x = x.replace(/[^.0-9 ]/g, "");
                y = y.replace(/[^.0-9 ]/g, "");
                //Sort items by price
                var s;
                if (value === "inc") {
                    if (parseFloat(x) > parseFloat(y)) {
                        s = weapons[i].innerHTML;
                        weapons[i].innerHTML = weapons[i + 1].innerHTML;
                        weapons[i + 1].innerHTML = s;
                        swapped = true;
                    }             
                }
                else if (value === "dec") {
                    if (parseFloat(x) < parseFloat(y)) {
                        s = weapons[i].innerHTML;
                        weapons[i].innerHTML = weapons[i + 1].innerHTML;
                        weapons[i + 1].innerHTML = s;
                        swapped = true;
                    }
                }
            }
        }
        while (swapped);
    }

    

    function sortByName(value) {
        //Sort name by A-Z
        do {
            swapped = false;
            for (var i = 0; i < weapons.length - 1; i++) {
                //Get names in lowerCase from childNodes
                var firstName = weapons[i].childNodes[1].childNodes[3].childNodes[1].childNodes[1].textContent.toLowerCase();
                var secondName = weapons[i + 1].childNodes[1].childNodes[3].childNodes[1].childNodes[1].textContent.toLowerCase();

                //Sort items by name
                var s;
                if (value === "inc") {
                    if (firstName > secondName) {
                        s = weapons[i].innerHTML;
                        weapons[i].innerHTML = weapons[i + 1].innerHTML;
                        weapons[i + 1].innerHTML = s;
                        swapped = true;
                    }
                }
                else if (value === "dec") {
                    if (firstName < secondName) {
                        s = weapons[i].innerHTML;
                        weapons[i].innerHTML = weapons[i + 1].innerHTML;
                        weapons[i + 1].innerHTML = s;
                        swapped = true;
                    }
                }
               
            }
        }
        while (swapped);
    }  
}
   
    
        

