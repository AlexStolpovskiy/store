﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.BLM;
using StoreWeapon.Models;

namespace StoreWeapon.Controllers
{
    namespace StoreWeapon.Controllers
    {
        public class StoreController : Controller
        {

            // GET: Store
            public ActionResult Index()
            {
                //TODO Automapping
                ViewBag.category = (Request.QueryString["Category"] ?? "All").Replace(" ",String.Empty);
                TempData["Category"] = ViewBag.category;
              
                var knivesForStore = new KnifeManager().GetKnivesByCategory(new string[] {ViewBag.category})
                    .Select(knife => new KnifeForStore
                    {
                        Id  = knife.GuidKnife,
                        Category = knife.Category,
                        Description = knife.Description,
                        LogoPath = knife.LogoPath,
                        Name = knife.Name,
                        NewPrice = knife.NewPrice,
                        OldPrice = knife.OldPrice
                    })
                    .ToList();

                return View(knivesForStore);
            }


            public PartialViewResult _WeaponList(string[] categories)
            {
               
                var knivesForStore = new KnifeManager().GetKnivesByCategory(categories)
                    .Select(knife => new KnifeForStore
                    {
                        Id = knife.GuidKnife,
                        Category = knife.Category,
                        Description = knife.Description,
                        LogoPath = knife.LogoPath,
                        Name = knife.Name,
                        NewPrice = knife.NewPrice,
                        OldPrice = knife.OldPrice
                    })
                    .ToList();

                return PartialView(knivesForStore);
            }

            public PartialViewResult _CategoriesPanel(string newCategory)
            {
  
                var categories = from faq in new KnifeManager().GetAllKnifesList() group faq by faq.Category into faqGroup select faqGroup;
                var category = categories.Select(cat => new StoreCategory() {Name = cat.Key, Count = cat.Count()}).ToList();

                return PartialView("_CategoriesPanel", category);
            }
        }
    }
}