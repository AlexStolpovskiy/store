﻿using StoreWeapon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL;
using System.Web.Mvc;

namespace StoreWeapon.Views
{
    public class CartController : Controller
    {
        // GET: Cart
        public ActionResult Index()
        {
            var cookiesResult = Request.Cookies["Knifes"].Value.ToString().Split(new String[]{"%2C"},StringSplitOptions.RemoveEmptyEntries);
            //TODO Automapper
            var result = new KnifeManager().GetKnifesByGuid(cookiesResult).Select(knife => new KnifeForStore
            {

                Id = knife.GuidKnife,
                Category = knife.Category,
                Description = knife.Description,
                LogoPath = knife.LogoPath,
                Name = knife.Name,
                NewPrice = knife.NewPrice,
                OldPrice = knife.OldPrice
            }).ToList(); ;
            return View(result);
        }

  
      
    }
}