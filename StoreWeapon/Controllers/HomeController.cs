﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Antlr.Runtime;
using BLL;
using StoreWeapon.Models;
using BLL.BLM;

namespace StoreWeapon.Controllers
{
    public class HomeController : Controller
    {
       //
        public ActionResult Index()
        {
            return View(GetAllCategories());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();

        }

        public List<MainCategory> GetAllCategories()
        {
            //TODO AUTO MAPPIN Hear
            var catList = new KnifeManager().GetAllCategoriesForMain();
            return catList.Select(category => new MainCategory(category.CategoryName, category.ImagePath)).ToList();
        }
    }
}